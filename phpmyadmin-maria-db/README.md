## Usage
Build:
```
docker-compose build
```

Run:
```
docker-compose up
```

## Volume
Volume named: mariadb-volume

## accessing maria db via mysql command

First do a docker container ls to get the name of the container:

```console
michael@michael-pc:~$ docker container ls
CONTAINER ID        IMAGE                   COMMAND                  CREATED             STATUS              PORTS                    NAMES
57551da53da5        phpmyadmin/phpmyadmin   "/docker-entrypoint.…"   2 minutes ago       Up 2 minutes        0.0.0.0:9090->80/tcp     phpmyadmin-maria-db_phpmyadmin_1
f893eee8bf6a        mariadb:10.5.3-bionic   "docker-entrypoint.s…"   2 minutes ago       Up 2 minutes        0.0.0.0:3306->3306/tcp   phpmyadmin-maria-db_mariadb_1

```

Then run the following command to get the ip of the container:
```console
michael@michael-pc:~$ docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' phpmyadmin-maria-db_mariadb_1 
172.24.0.3
```

Access the database with mysql command:
```
mysql -h 172.24.0.3 -u root -p
```

## Accessing phpmyadmin
The web interface can be accessed using the following link:
>localhost:9090
